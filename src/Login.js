import React, { Component } from 'react';
import './App.css';
import { Button, Container, Row, Col, Card, Form } from 'react-bootstrap';
import axios from'axios';

class Login extends Component {
    constructor(props){
    super(props)
    this.state = {
      error: null,
      email: '',
      password: ''
    }
    this.handleChangeEmail = this.handleChangeEmail.bind(this);
    this.handleChangePass = this.handleChangePass.bind(this);
    }
    handleChangeEmail(e){
        this.setState({email: e.target.value});
    }
    handleChangePass(e){
        this.setState({password: e.target.value});
    }
    onSubmit = (router) => {
        const data = {
            email: this.state.email,
            password: this.state.password
        }
        axios.post("https://vskvietnam.vn/api/users/login", data)
        .then((result) => {
            console.log(result)
            alert('Login thanh cong')
            localStorage.setItem('ID', result.data.id);
            this.props.history.push(router)
        })
          .catch((error) => {
              alert('Login that bai')
          });
    }
    
    render(){
      return (
        <Container>
        <hr />
        <Form>
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control type="email" placeholder="Enter email" value={this.state.email} onChange={this.handleChangeEmail} />
            <Form.Text className="text-muted">
              We'll never share your email with anyone else.
            </Form.Text>
          </Form.Group>
  
          <Form.Group controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Password" value={this.state.password} onChange={this.handleChangePass} />
          </Form.Group>
          <Form.Group controlId="formBasicCheckbox">
            <Form.Check type="checkbox" label="Check me out" />
          </Form.Group>
          <Button variant="primary" onClick={() => this.onSubmit('/')}> 
            Login
          </Button>
      
        </Form>
      </Container>
      );
  }
  }
  export default Login;