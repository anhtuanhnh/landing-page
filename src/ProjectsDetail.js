import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Button, Container, Row, Col, Card } from 'react-bootstrap';
import axios from'axios';
import { Link, useParams } from "react-router-dom";


function ListItems({ value }) {
  return <Col md={12} xs={12}>
    <Card style={{}}>
      <Card.Img variant="top" src={value.image_url} />
      <Card.Body>
        <Card.Title>{value.title}</Card.Title>
        <Card.Text>
         {value.description}
    </Card.Text>
        <Link to={""} >Xem chi tiết</Link>
      </Card.Body>
    </Card>
    {/* <p>{value.title}</p>
    <img src={value.image_url} style={{ height: 50 }} /> */}
  </Col>
}

class ProjectsDetail extends Component {
    constructor(props){
    super(props)
    this.state = {
      error: null,
      detail: null,
      fetching: false
    }
    }
    componentDidMount(){ 
      const id = this.props.match.params.id
      this.setState({fetching: true})
      axios.get(`https://vskvietnam.vn/api/projects/${id}`)
      .then((result) => {
        this.setState({
          detail: result.data,
          fetching: false
        });
        })
        .catch((error) => {
          this.setState({
            error
          })
        });
    }
    render(){
      const {detail, error} = this.state;
      console.log(this.props)
      if(error){
        return <div>Error: {error.message}</div>;
      }
      else{
        return (
          <Container style={{paddingBottom: 100}} >
            {
              this.state.fetching && <h1>Loading</h1>
            }
            <h1>News Detail</h1>
            {
              detail && <Row>
              <ListItems value={detail} />
            </Row>
            }
          </Container>
        );
    }
  }
  }
  export default ProjectsDetail;
