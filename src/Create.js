import React, { Component } from 'react';
import './App.css';
import { Button, Container, Row, Col, Card, Form } from 'react-bootstrap';
import axios from 'axios';

class Create extends Component {
    constructor(props) {
        super(props)
        this.state = {
            error: null,
            email: '',
            password: '',
            confirmpassword: ''
        }
        this.handleChangeEmail = this.handleChangeEmail.bind(this);
        this.handleChangePass = this.handleChangePass.bind(this);
        this.handleChangeConfirm = this.handleChangeConfirm.bind(this);
    }
    handleChangeEmail(e) {
        this.setState({ email: e.target.value });
    }
    handleChangePass(e) {
        this.setState({ password: e.target.value });
    }
    handleChangeConfirm(e) {
        this.setState({ confirmpassword: e.target.value });
    }
    onSubmit = () => {
        const data = {
            email: this.state.email,
            password: this.state.password
        }
        if ((this.state.password === this.state.confirmpassword) && (this.state.password.length > 7)){
            axios.post("https://vskvietnam.vn/api/users", data)
            .then((result) => {
                console.log(result)
                alert('Create thanh cong');
                this.props.history.push('/login');
            })
            .catch((error) => {
                alert('Create that bai')
            });         
        }
        else{
            alert('Ban nhap sai cu phap')
        }
    }
    render() {
        return (
            <Container>
                <hr />
                <Form>
                    <Form.Group controlId="formBasicEmail">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email" placeholder="Enter email" value={this.state.email} onChange={this.handleChangeEmail} />
                        <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
            </Form.Text>
                    </Form.Group>

                    <Form.Group controlId="formBasicPassword">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" value={this.state.password} onChange={this.handleChangePass} />
                    </Form.Group>
                    <Form.Text className="text-muted">
                        >= 8 characters
          </Form.Text>

                    <Form.Group controlId="formBasicConfirmPassword">
                        <Form.Label>Confirm Password</Form.Label>
                        <Form.Control type="password" placeholder="Confirm Password" value={this.state.confirmpassword} onChange={this.handleChangeConfirm} />
                    </Form.Group>
                    <Form.Text className="text-muted">
                        Same as Password.
          </Form.Text>
                    <hr />
                    <Form.Group controlId="formBasicCheckbox">
                        <Form.Check type="checkbox" label="Check me out" />
                    </Form.Group>
                    <Button variant="primary" onClick={this.onSubmit} >
                        Submit
          </Button>

                </Form>
            </Container>
        );
    }
}
export default Create;