import React, { Component, createRef } from 'react';
import { Container, Row, Col } from 'react-bootstrap';



const About = React.forwardRef((props, ref) => {
        return(
        <Container ref={ref}>
            <div className="about__content">
                <div className="about__title">
                    <h3>ABOUT ME</h3>
                    <span>Main informations about me</span>
                </div>
                <div className="info__about__me">
                   <Row>
                   <div className="info_photo">
                        <img className="info_photo_item" src={require('../assets/images/006.jpg')}
                            alt="MyPhoto" />

                    </div>

                    <div className="text__about__me">
                        <div className="subTitle__aboutMe">
                            <h6>I'm John Wilson and I'm </h6>
                        </div>
                        <div className="paragraph__aboutMe ">
                            I am a Web Developer, Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                            Lorem Ipsum has been the industry's standard dummy text ever since.
                            Lorem Ipsum has been the industry.
                            </div>
                        <div className="more_info_aboutMe">
                            <ul>
                                <li>
                                    <label>Birthday:</label>
                                    <p>13.09.1996</p>
                                </li>
                                <li>
                                    <label>City:</label>
                                    <p>Kharkov, Ukraine</p>
                                </li>
                            </ul>
                            <ul>
                                <li>
                                    <label>Mail:</label>
                                    <p><a href="#">myMail@gmail.com</a></p>
                                </li>
                                <li>
                                    <label>Phone:</label>
                                    <p><a href="#">+380 00 000 0000</a></p>
                                </li>
                            </ul>
                        </div>
                        <div className="link_social_aboutMe">
                            <a title="facebook" href="#">
                                <i className="fab fa-facebook-f" aria-hidden="true">
                                </i>
                            </a>
                            <a title="twitter" href="#">
                                <i className="fab fa-twitter" aria-hidden="true">
                                </i>
                            </a>
                            <a title="linkedin" href="#">
                                <i className="fab fa-linkedin-in" aria-hidden="true">
                                </i>
                            </a>
                            <a title="instagram" href="#">
                                <i className="fab fa-instagram" aria-hidden="true">
                                </i>
                            </a>
                            <a title="dribbble" href="#">
                                <i className="fab fa-dribbble" aria-hidden="true">
                                </i>
                            </a>
                        </div>
                  
                        <div className="btn__aboutMe">
                            <div className="down" >
                            <a download href="text.txt">
                                Download CV
                                    </a>
                                    </div>
                                    <div className="send">
                            <a  href="#contact">
                                Send Message
                                    </a>
                                    </div>
                    
                        </div>
                    </div>
                   </Row>
                    

                </div>
                
                <div className="myStoryAboutMe">
                    <div className="myStory">
                        <h5>My Story</h5>
                        <p>Hi! My name is John Wilson. I am a Web Developer,
                        I am a Web Developer,I am a Web Developer,I am a Web Developer,
                        I am a Web Developer,I am a Web Developer,I am a Web Developer,
                                I am a Web Developer,I am a Web Developer,I am a Web Developer, </p>
                    </div>
                    <div className="mySkills">
                        <h5>My Skills</h5>
                        <div className="skill-title">
                            <label>HTML/CSS</label>
                            <label data-num="85" className="skill-bar-percent"></label>
                            <label>85%</label>

                        </div>
                        <div className="skillbar" data-percent="85%">
                            <div className="skill-bar one" style={{ width: 85 + '%' }}></div>

                        </div>
                        <div className="skill-title">
                            <label>UI/UX</label>
                            <label data-num="90" className="skill-bar-percent"></label>
                            <label>90%</label>

                        </div>
                        <div className="skillbar" data-percent="90%">
                            <div className="skill-bar one" style={{ width: 90 + '%' }}></div>

                        </div>
                        <div className="skill-title">
                            <label>JAVASCRIPT</label>
                            <label data-num="55" className="skill-bar-percent"></label>
                            <label>55%</label>

                        </div>
                        <div className="skillbar" data-percent="55%">
                            <div className="skill-bar one" style={{ width: 55 + '%' }}></div>

                        </div>
                        <div className="skill-title">
                            <label>PHP</label>
                            <label data-num="40" className="skill-bar-percent"></label>
                            <label>40%</label>

                        </div>
                        <div className="skillbar" data-percent="40%">
                            <div className="skill-bar one" style={{ width: 40 + '%' }}></div>

                        </div>

                    </div>
                </div>

            </div>
        </Container>
);
})
export default About;