import React, { Component, createRef } from 'react';

const Contact = React.forwardRef((props, ref) => {
    
        return (
            <div ref={ref}>
                <div>
                <div className="contact_content">
                    <div className="about__title">
                        <h3>Contact</h3>
                        <span>Get in touch</span>
                    </div>
                    <div className="contact-title">
                        <span>Leave a message</span>
                    </div>
                    <div className="contact__conte">
                        <div className="info-contact">
                            <p>Kharkov Ukraine</p>
                            <a href="#">myMail@gmail.com</a>
                            <a href="#">+380 00 000 0000</a>
                        </div>
                        <div className="contact-form">
                            <form name="MyForm">
                                <input type="hidden" name="project_name" value="Site Name"></input>
                                <input type="hidden" name="admin_email" value="yourEmail@gmail.com"></input>
                                <input type="hidden" name="form_subject" value="Form"></input>
                                <div className="form-name">
                                    <input type="email" id="name" name="Name" placeholder="Enter your name" />
                                </div>
                                <div className="form-email">
                                    <input type="email" id="name" name="Name" placeholder="Enter your email" />
                                </div>
                                <div className="form-textarea">
                                    <textarea name="text_comment" id="text_comment" placeholder="How can I help you?"></textarea>
                                </div>
                                <div className="btn__send">
                                    <button type="submit" id="btn_submit" name="done">
                                        <span>Send</span>
                                    </button>
                                </div>
                            </form>
                            <div id="winError"></div>
                        </div>
                    </div>
                    </div>
                    <div className="footer">
                    <p>© 2019 All Rights Reserved</p>
                </div>
                </div>
         
                
            </div>
        );
    })
export default Contact;