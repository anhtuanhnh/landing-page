import React, { Component, createRef } from 'react';
import './Home.css';
import About from './About';
import Services from './Services';
import Portflolio from './Portflolio';
import Contact from './Contact';
// import Ref from './Ref';


class Home extends Component {


    state = {
        hideMenu: false
    }

    toggleMenu = () => {
        this.setState({hideMenu: !this.state.hideMenu})
    }

    session1 = createRef()
    session2 = createRef()
    session3 = createRef()
    session4 = createRef()
    session5 = createRef()
    session6 = createRef()
    session7 = createRef()



    moveToSession1 = () => {
        this.session1.current.scrollIntoView({
            behavior: 'smooth',
            block: 'start',
          });
    }
    
    moveToSession2 = () => {

        try{
            this.session2.current.scrollIntoView({
                behavior: 'smooth',
                block: 'start',
            });
        } catch(e){
            console.log(e)
        }
    }

    moveToSession3 = () => {
        this.session3.current.scrollIntoView({
            behavior: 'smooth',
            block: 'start',
          });
    }

    moveToSession4 = () => {
        this.session4.current.scrollIntoView({
            behavior: 'smooth',
            block: 'start',
          });
    }
    moveToSession5 = () => {
        this.session5.current.scrollIntoView({
            behavior: 'smooth',
            block: 'start',
          });
    }
    moveToSession6 = () => {
        this.session6.current.scrollIntoView({
            behavior: 'smooth',
            block: 'start', 
          });
    }
    moveToSession7 = () => {
        this.session7.current.scrollIntoView({
            behavior: 'smooth',
            block: 'start',
          });
    }
    render() {
        return (
            <div>
                <div className={`menu ${this.state.hideMenu && "hide"}`}>
                    <div className="logo">
                        <a href="">
                            <img src={require('../assets/images/logo.png')} alt="logo" />
                        </a>
                    </div>
                    <div className="nav">
                        <ul>
                            <li><a onClick={this.moveToSession1}>HOME</a></li>
                            <li><a onClick={this.moveToSession2}>ABOUT</a></li>
                            <li><a onClick={this.moveToSession3}>SERVICES</a></li>
                            <li><a onClick={this.moveToSession4}>PORTFOLIO</a></li>
                            <li><a onClick={this.moveToSession5}>CONTACT</a></li>
                        </ul>
                        
                    </div>
                    <div className={`close__menu ${this.state.hideMenu && "close__menu__right"}`} onClick={this.toggleMenu}>
                        <i className={`fas fa-chevron-left ${this.state.hideMenu && "fas fa-chevron-right"}`} onClick={this.toggleMenu} aria-hidden="true" />
                    </div>
                </div>
                
                    <div className={`content ${this.state.hideMenu && "full-width"}`}  ref={this.session1}>
                        <div className="move_content">
                            <header className="header">
                                <div className="header__content">
                                    <div className="header__title">
                                        <h1>PORTFOLIO</h1>
                                        <h2>Hello I'm </h2>
                                    </div>
                                </div>
                            </header>
                     <ul>
                         
                        <><About ref={this.session2} /></>
                        <><Services ref={this.session3} /></>
                        <><Portflolio ref={this.session4} /></>
                        <><Contact ref={this.session5} /></>
                        {/* <><Ref ref={this.session6} /></> */}

                        </ul>
                        
                    </div>
                  
                </div>
                <a className="move-top" onClick={this.moveToSession1}>
                    <i className="fas fa-angle-up" aria-hidden="true"></i>
                    </a>
            </div>
        )
    }
}
export default Home;