import React, { Component } from 'react';
import Tabs from './Tabs';




const Services = React.forwardRef((props, ref) => {
    return (
        <div ref={ref}>
            <div>
            <div className="services__content">
                
                <div className="about__title">
                    <h3>Services</h3>
                    <span>What i do</span>
                </div>
                <div>
                    <div className="services_items">
                        <div className="service_iner">
                            <div className="service-title">
                                <div className="service_icon">
                                    <img src={require('../assets/images/logo.png')} alt="icon" />
                                </div>
                                <span>UX</span>
                            </div>
                            <div className="service-subTitle">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    Lorem Ipsum has Ipsum has been the.
                        </p>
                            </div>
                        </div>
                        <div className="service_iner">
                            <div className="service-title">
                                <div className="service_icon">
                                    <img src={require('../assets/images/logo.png')} alt="icon" />
                                </div>
                                <span>DESIGN</span>
                            </div>
                            <div className="service-subTitle">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    Lorem Ipsum has Ipsum has been the.
                        </p>
                            </div>

                        </div>
                        <div className="service_iner">
                            <div className="service-title">
                                <div className="service_icon">
                                    <img src={require('../assets/images/logo.png')} alt="icon" />
                                </div>
                                <span>CODING</span>
                            </div>
                            <div className="service-subTitle">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    Lorem Ipsum has Ipsum has been the.
                        </p>
                            </div>

                        </div>
                        <div className="service_iner">
                            <div className="service-title">
                                <div className="service_icon">
                                    <img src={require('../assets/images/logo.png')} alt="icon" />
                                </div>
                                <span>SOCIAL</span>
                            </div>
                            <div className="service-subTitle">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    Lorem Ipsum has Ipsum has been the.
                        </p>
                            </div>

                        </div>
                        <div className="service_iner">
                            <div className="service-title">
                                <div className="service_icon">
                                    <img src={require('../assets/images/logo.png')} alt="icon" />
                                </div>
                                <span>SEO</span>
                            </div>
                            <div className="service-subTitle">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    Lorem Ipsum has Ipsum has been the.
                        </p>
                            </div>

                        </div>
                        <div className="service_iner">
                            <div className="service-title">
                                <div className="service_icon">
                                    <img src={require('../assets/images/logo.png')} alt="icon" />
                                </div>
                                <span>COMMERCE</span>
                            </div>
                            <div className="service-subTitle">
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                    Lorem Ipsum has Ipsum has been the.
                        </p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div className="counter__content">
                <div className="counter">
                    <div className="bl">
                        <div>
                            <i className="fas fa-users" aria-hidden="true">
   
                            </i>
                            <h4 data-num="650">650</h4>
                            <p>Happy Clients</p>
                        </div>
                    </div>
                    <div className="bl">
                        <div>
                            <i className="fas fa-thumbs-up" aria-hidden="true">

                            </i>
                            <h4 data-num="635">635</h4>
                            <p>Projects Completed</p>
                        </div>
                    </div>
                    <div className="bl">
                        <div>
                            <i className="fas fa-terminal" aria-hidden="true">

                            </i>
                            <h4 data-num="1250">1250</h4>
                            <p>Lines Of Code</p>
                        </div>
                    </div>
                    <div className="bl">
                        <div>
                            <i className="fas fa-trophy" aria-hidden="true">

                            </i>
                            <h4 data-num="420">420</h4>
                            <p>Awards Achieved</p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="works__content">
                <div className="about__title">
                    <h3>PORTFOLIO</h3>
                    <span>Work i have done</span>
                </div>

                <Tabs>
                    <div label="ALL">
                        <div className="works__inner">
                            <div className="work_item filter application" style={{}}>
                                <div className="item_content">
                                    <img src={require('../assets/images/001.jpg')} alt="work" />
                                    <div className="item-overlay">
                                   <div className="item_title">
                                        <h6>Application</h6>
                                        <a href={require('../assets/images/001.jpg')}>
                                            <i className="fas fa-search" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div className="work_item filter application" style={{}}>
                                <div className="item_content">
                                    <img src={require('../assets/images/003.jpg')} alt="work" />
                                    <div className="item-overlay">

                                    

                                    <div className="item_title">
                                        <h6>Application</h6>
                                        <a href={require('../assets/images/001.jpg')}>
                                            <i className="fas fa-search" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            <div className="work_item filter application" style={{}}>
                                <div className="item_content">
                                    <img src={require('../assets/images/005.jpg')} alt="work" />
                                    <div className="item-overlay">

                                    

                                    <div className="item_title">
                                        <h6>Application</h6>
                                        <a href={require('../assets/images/001.jpg')}>
                                            <i className="fas fa-search" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div label="WEB DESIGN">
                        <div className="works__inner">
                            <div className="work_item filter application" style={{}}>
                                <div className="item_content">
                                    <img src={require('../assets/images/0069.jpg')} alt="work" />
                                    <div className="item-overlay">

                                   

                                    <div className="item_title">
                                        <h6>Application</h6>
                                        <a href={require('../assets/images/001.jpg')}>
                                            <i className="fas fa-search" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div label="APPLICATION">
                        <div className="works__inner">
                            <div className="work_item filter application" style={{}}>
                                <div className="item_content">
                                    <img src={require('../assets/images/008.jpg')} alt="work" />
                                    <div className="item-overlay">

                                   

                                    <div className="item_title">
                                        <h6>Application</h6>
                                        <a href={require('../assets/images/001.jpg')}>
                                            <i className="fas fa-search" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div label="DEVELOPMENT">
                        <div className="works__inner">
                            <div className="work_item filter application" style={{}}>
                                <div className="item_content">
                                    <img src={require('../assets/images/007.jpg')} alt="work" />
                                    <div className="item-overlay">

                                  

                                    <div className="item_title">
                                        <h6>Application</h6>
                                        <a href={require('../assets/images/001.jpg')}>
                                            <i className="fas fa-search" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                      </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Tabs>



            </div>
            </div>
        </div>
    );
})
export default Services;