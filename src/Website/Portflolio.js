import React, { Component } from 'react';
import Carousel from 'react-bootstrap/Carousel'

const Portflolio = React.forwardRef((props, ref) => {
  return (
    <div ref={ref}>
      <div className="feedback_content">
        <div className="feedback_blur">
      <Carousel>
        <Carousel.Item>
          <div
            className="d-block"
            
          ></div>
          <Carousel.Caption>
            <img className="radius" src={require('../assets/images/face.jpg')} alt="First slide" />
            <h3>Mary Brown</h3>
            <p>Dolor facilis veritatis doloremque dicta eos 
              Voluptate earum nulla ad et esse Saepe asperiores nisi 
              facere ipsam corporis. Dolorem praesentium tenetur tempore 
              dolorem illum autem? Veritatis fuga quasi sunt tenetur. 
              Expedita id eaque incidunt beatae nesciunt! In similique 
              exercitationem tempore excepturi placeat Nostrum ducimus dicta temporibus quas!</p>
            
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
        <div
            className="d-block"
            
          ></div>

          <Carousel.Caption>
          <img className="radius" src={require('../assets/images/face.jpg')} alt="First slide" />
            <h3>Mary Brown</h3>
            <p>Dolor facilis veritatis doloremque dicta eos 
              Voluptate earum nulla ad et esse Saepe asperiores nisi 
              facere ipsam corporis. Dolorem praesentium tenetur tempore 
              dolorem illum autem? Veritatis fuga quasi sunt tenetur. 
              Expedita id eaque incidunt beatae nesciunt! In similique 
              exercitationem tempore excepturi placeat Nostrum ducimus dicta temporibus quas!</p>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
        <div
            className="d-block"
            
          ></div>

          <Carousel.Caption>
          <img className="radius" src={require('../assets/images/face.jpg')} alt="First slide" />
            <h3>Mary Brown</h3>
            <p>Dolor facilis veritatis doloremque dicta eos 
              Voluptate earum nulla ad et esse Saepe asperiores nisi 
              facere ipsam corporis. Dolorem praesentium tenetur tempore 
              dolorem illum autem? Veritatis fuga quasi sunt tenetur. 
              Expedita id eaque incidunt beatae nesciunt! In similique 
              exercitationem tempore excepturi placeat Nostrum ducimus dicta temporibus quas!</p>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
      </div>
      </div>
      </div>
  );
})

export default Portflolio;