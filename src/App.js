import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";


import News from "./News";
import NewsDetail from "./NewsDetail";
import Projects from "./Projects";
import ProjectsDetail from "./ProjectsDetail";
import Login from "./Login";
import Create from './Create';
import List from './Website/List';

import Home from "./Website/Home";
// This site has 3 pages, all of which are rendered
// dynamically in the browser (not server rendered).
//
// Although the page does not ever refresh, notice how
// React Router keeps the URL up to date as you navigate
// through the site. This preserves the browser history,
// making sure things like the back button and bookmarks
// work properly.

export default function BasicExample() {
  return (
    <Router>
      <div>
        {/* <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/list">List</Link>
          </li>
          <li>
            <Link to="/news">News</Link>
          </li>
          <li>
            <Link to="/projects">Projects</Link>
          </li>
          <li>
            <Link to="/login">Login</Link>
          </li>
          <li>
            <Link to="/create">Create</Link>
          </li>
        </ul> */}


        {/*
          A <Switch> looks through all its children <Route>
          elements and renders the first one whose path
          matches the current URL. Use a <Switch> any time
          you have multiple routes, but you want only one
          of them to render at a time
        */}
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/list" component={List} />

          

          {/* <Route exact path="/news" component={News} />
          <Route path="/news/detail/:id" component={NewsDetail} />
          <Route exact  path="/projects" component={Projects} />
          <Route path="/projects/detail/:id" component={ProjectsDetail} />
          <Route path="/login" component={Login} />
          <Route path="/create" component={Create} /> */}

        </Switch>
      </div>
    </Router>
  );
}

// You can think of these components as "pages"
// in your app.
