import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Button, Container, Row, Col, Card } from 'react-bootstrap';
import axios from'axios';
import { Link } from "react-router-dom";


function ListItems({ value }) {
  return <Col md={3} xs={12}>
    <Card style={{}}>
      <Card.Img variant="top" src={value.image_url} style={{ height: 200 }} />
      <Card.Body>
        <Card.Title>{value.title}</Card.Title>
        <Card.Text>
         {value.description}
    </Card.Text>
        <Link to={`/news/detail/${value.id}`} >Xem chi tiết</Link>
      </Card.Body>
    </Card>
    {/* <p>{value.title}</p>
    <img src={value.image_url} style={{ height: 50 }} /> */}
  </Col>
}

class News extends Component {
    constructor(props){
    super(props)
    this.state = {
      error: null,
      list: [],
      fetching: false
    }
    }
    componentDidMount(){ 
      this.setState({fetching: true})
      axios.get("https://vskvietnam.vn/api/news")
      .then((result) => {
        this.setState({
          list: result.data,
          fetching: false
        });
        })
        .catch((error) => {
          this.setState({
            error
          })
        });
    }
    render(){
      const {list, error} = this.state;
      //console.log(this.props); hiển thị trong f12 có những gì
      if(error){
        return <div>Error: {error.message}</div>;
      }
      else{
      return (
        <Container>
  
          {
            this.state.fetching && <h1>Loading</h1>
          }
          <h1>List News</h1>
          <Row>
            {list && list.map((item) =>
              <ListItems key={item.id} value={item} />
            )}
          </Row>
        </Container>
      );
    }
  }
  }
  export default News;
